#!/usr/bin/env python3
#import warnings
#warnings.filterwarnings("ignore")

import urllib.request
from bs4 import BeautifulSoup

def get_html(url):
    response = urllib.request.urlopen(url)
    return response.read()

def parse(html):
	soup = BeautifulSoup(html, "lxml")
	table = soup.find('div', class_= 'posts shortcuts_items')

	projects = []

	for row in table.find_all('div'):
		cols = row.find('div' , class_='post__header')
		
		if cols != None :		
			projects.append({
				'titile': cols.find_all('a')[1].text,
				'categories': cols.find_all('a')[0].text,
				'link': cols.find_all('a', href=True)[1]['href']
			})
	for project in projects:
		print(project)

def main():
	parse(get_html('https://habrahabr.ru/'))
	parse(get_html('https://habrahabr.ru/page2'))	

if __name__ == '__main__':
    main()

