import sys

mass = [128,64,32,16,8,4,2,1]

def sumOneNumber(number):
	x = 0
	result = ""
	while x != len(mass):
		if number >= mass[x]:
			number = number - mass[x]
			result = result + '1'
		else: result = result + '0'	
		x = x + 1;
	return result

def sumFewNumbers():
	result = ""
	for x in b:
		result = result + str(sumOneNumber(int(x)) + ".")

	result = result[:-1]	
	return result


#for version Python3-
#line = raw_input()

line = input()

if len(line) > 3:
	b = line.split('.')
	print(sumFewNumbers())
else:
	print(sumOneNumber(int(line)))
